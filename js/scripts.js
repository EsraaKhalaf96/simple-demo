$(document).ready(function () {
  // dropdown Initialization
  $('.dropdown-button').dropdown({
    inDuration: 300,
    outDuration: 225,
    constrainWidth: false, // Does not change width of dropdown to that of the activator
    hover: true, // Activate on hover
    gutter: 0, // Spacing from edge
    belowOrigin: true, // Displays dropdown below the button
    alignment: 'left', // Displays dropdown with edge aligned to the left of button
    stopPropagation: false // Stops event propagation
  });
  $('.fa.fa-bookmark, .flageedtext').click(function () {
    $(".fa.fa-bookmark, .flageedtext").toggleClass('flaggedColor');
    console.log('test')
  });

  $(".closeIcon").click( function(){
    $('.tabsBlock' ).toggleClass('tableft');
    $(".questionsBlock").toggleClass('quesBlock');
  });
  // $('.tabsBlock').slideToggle();

  $('.tabcont').on('click', function () {
    $('.tabcont').removeClass('tabActive');
    $(this).addClass('tabActive');
    var tab_id = $(this).attr('data-tab');
    $('.tabcont').removeClass('current');
		$('.tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
    // $('.contnetTab .tabcontent').removeClass('contentActive');
    // var i = $(this).index();
    // $('.contnetTab .tabcontent').eq(i).addClass('contentActive');
  });


  // counter

  var count = 1;
  var counter = setInterval(timer, 50);

  function timer() {
    count = count + 1;
    if (count == +1) {
      clearInterval(counter);
      return;
    } else if (count == 1800) {
      $('.timer').css("color", "#f0c826");
    } else if (count == 2700) {
      $('.timer').css("color", "#f04c26");
    } else if (count == 3600) {
      clearInterval(counter);
    }
    var seconds = count % 60;
    var minutes = Math.floor(count / 60);
    var hours = Math.floor(minutes / 60);
    minutes %= 60;
    hours %= 60;
    hour = ("00" + hours).substr(-2);
    min = ("00" + minutes).substr(-2);
    sec = ("00" + seconds).substr(-2);
    document.getElementById("timer").innerHTML = hour + ":" + min + ":" + sec; // watch for spelling
  }


});